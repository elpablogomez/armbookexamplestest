#!/bin/bash
for DIR in `ls -d */`;
do
    echo "checking location: " $DIR;
    cd $DIR
    if [ -f main.cpp ]
    then
        echo "updating location: " $DIR;
        cd ..
        git subtree pull --prefix $DIR https://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@github.com/armBookCodeExamples/$DIR master --squash -m "initial commits"
    else
        cd ..
    fi
    echo " ";
done